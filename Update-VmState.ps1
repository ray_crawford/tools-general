<#
.SYNOPSIS
  Start, Stop or List vms in a Resource Group.

.DESCRIPTION
  Start, Stop or List vms in a Resource Group.

.PARAMETER RGName
    The name of the Resource Group to alter or query.
    
.PARAMETER Stop
    Switch to stop all VMs in a resource group (optional).

.PARAMETER Start
    Switch to start all VMs in a resource group (optional).

.PARAMETER List
    Switch to list all VMs in a resource group (optional, default).
    
.INPUTS
  An AzureRmAccount authorization.

.OUTPUTS
  List of VMs in the given resource group

.NOTES
  Version:        1.0
  Author:         Ray Crawford
  Creation Date:  4/22/2016
  Purpose/Change: Initial script development
  Pipeline: query for a tenant and subscription ID if multiple are present in current account using grid-view like in the Add-Users PowerShell script.
  
.EXAMPLE
  Update-VmState -RGName $RGName -List

#>

function Update-VmState () {
[CmdletBinding()]
Param(
  [Parameter(Mandatory=$False,Position=1)]
  [string]$RGName,
  [switch]$List,
  [switch]$Start,
  [switch]$Stop
)

# Make the -List switch the default
if (($Start -eq $false) -and ($Stop -eq $false)) {
    $List = $true
}

# Confirm that the user is authenticated to Azure fabric
Try {
    Get-AzureRmSubscription -ErrorAction Stop
}
Catch {
    $Message = "Azure ARM account issue; you're likely not logged in.  Use Login-AzureRmAccount." 
    Write-Warning $Message
    Write-Verbose $_.Exception
    return
}
Finally {}
    if ( ($RGName -eq "all") -or (! $RGName ) ) {
        Write-Host "Getting all VMs :" -ForeGround "yellow" 
        Write-Host " => in subscription b32a6185-0803-494b-83dd-6839ead0eb58" -ForeGround "yellow"
        Write-Host " => in Tenant 3596192b-fdf5-4e2c-a6fa-acb706c963d8" -Foreground "yellow"
        Set-AzureRmContext -TenantID "3596192b-fdf5-4e2c-a6fa-acb706c963d8" -SubscriptionId "b32a6185-0803-494b-83dd-6839ead0eb58">$null
        $vms = get-azurermvm
    } else {
        Write-Host "Getting all VMs :" -ForeGround "yellow" 
        Write-Host " => in Resource Group ${RGName}" -ForeGround "yellow"
        Write-Host " => in subscription b32a6185-0803-494b-83dd-6839ead0eb58" -ForeGround "yellow"
        Write-Host " => in Tenant 3596192b-fdf5-4e2c-a6fa-acb706c963d8" -Foreground "yellow"
        Set-AzureRmContext -TenantID "3596192b-fdf5-4e2c-a6fa-acb706c963d8" -SubscriptionId "b32a6185-0803-494b-83dd-6839ead0eb58">$null
        $vms = get-azurermvm -ResourceGroupName $RGName
    }
    if ($Start) {
        $vms | `
        ForEach-Object {
            Write-Output "Start:$($_.Name)"
            $_ | Start-AzureRmVM
# I'd like to implement, but Start-AzureRmVm is a blocking call...
#            Write-Host -NoNewLine "Starting"
#            do {
#                        $vm = $_ | Get-AzureRmVM -Status
#                        Start-Sleep -Seconds 3
#                        Write-Host -NoNewline "."
#                    } until ($vm.Statuses[1].Code -eq 'PowerState/running')
#            Write-Output "$($_.Name) Started`n"
        }
    }
    if ($Stop) {
        $vms | `
        ForEach-Object {
            Write-Output "Stop: $($_.Name)"
            $_ | Stop-AzureRmVM -Force
# I'd like to implement, but Stop-AzureRmVm is a blocking call...
#            Write-Host -NoNewLine "Stopping"
#            do {
#                        $vm = $_ | Get-AzureRmVM -Status
#                        Start-Sleep -Seconds 3
#                        Write-Host -NoNewline "."
#                    } until ($vm.Statuses[1].Code -eq 'PowerState/deallocated')
#            Write-Output "$($_.Name) Stopped`n"
        }
    }
    if ($List) {
        $initialState = @{}
        foreach ($VM in $VMs) {
            $initialState[$VM.Name] = @{}
            $initialState[$VM.Name].Add("IP", (Get-AzureRmNetworkInterface | Where-Object {$_.Id -eq $VM.NetworkInterfaceIDs}).IpConfigurations.PrivateIpAddress)
            $initialState[$VM.Name].Add("OS", $VM.StorageProfile.OsDisk.OsType)
            $initialState[$VM.Name].Add("RGName", $VM.ResourceGroupName)
            $statuses = $VM | Get-AzureRmVM -status
            if ($statuses.Statuses[1].Code -eq 'PowerState/running') {
                $initialState[$VM.Name].Add("State", 'running')
            } else {
                $initialState[$VM.Name].Add("State", 'stopped')
            }
        }

        Write-Host "`n`n############################################################" -Foreground "yellow"
        foreach ($vmName in $initialState.keys) {"{0}`t{1}`t{2}`t{3}`t{4}" -f $vmName, $initialState.$vmName.RGName, $initialState.$vmName.OS, $initialState.$vmName.IP, $initialState.$vmName.State }
        Write-Host "############################################################`n" -Foreground "yellow"
    }
}

